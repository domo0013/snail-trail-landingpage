# www.snailtrail.fun

https://www.freecodecamp.org/news/how-to-host-a-git-repository-on-a-subdomain-with-netlify-b8afb5fca96e/


https://youtu.be/8_4h2DTl9co

# Connect Azure Devops repo to Netlify
https://community.netlify.com/t/support-guide-how-to-connect-azure-devops-repositories-to-netlify/13040
# tailwind-css

[![Netlify Status](https://app.netlify.com/sites/gamific/deploys)](https://app.netlify.com/sites/gamific/deploys)

A very simple starter set up with [TailwindCSS](https://tailwindcss.com/) and its [typography plugin](https://tailwindcss.com/docs/typography-plugin) and a build setup using [PostCSS](https://postcss.org/) and PurgeCSS (when running the production build).

In the preview deployment on Netlify it currently has a 100 score on both mobile and desktop on [Google PageSpeed](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Flucid-nightingale-60a4e2.netlify.app%2F&tab=mobile).


This setup can be used both as a starter project and a theme.

## As a Project

```bash
npm install
hugo server
```

## As a Theme

```bash
hugo mod npm pack
npm install
```

You need to add this to your `config.toml` (the stats are used by the CSS purging):

```toml
[build]
writeStats = true
```

Then run your project as usual.
